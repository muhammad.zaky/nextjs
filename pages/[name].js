import axios from "axios";
import styles from "../styles/Home.module.css";
import Image from 'next/image'

const PokemonById = ({ name, pokemon }) => {
  const pokemonPhoto = pokemon.sprites.other["official-artwork"].front_default;
  const { abilities, base_experience } = pokemon;
  

  return (
    <div className={styles.container}>
      <div className={styles.card}>
        <h1>{name}</h1>
        <div className={styles.pokemonPhoto}>
        <Image src={pokemonPhoto} width={300} height={250} alt={name}/>
        </div>
        <div className={styles.abilities}>
          {abilities.filter(item => item.is_hidden === false).map((itemAbility, index) => {
            return <div className={styles.abilitiesDetail} key={index}>{itemAbility.ability.name}</div>
          })}
          <div className={styles.experience}>{base_experience}</div>
        </div>
      </div>
    </div>
  );
};

export const getStaticProps = async (ctx) => {
  const name = ctx.params.name;
  const pokemon = await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`)
  console.log(pokemon)
  return {
    props: {
      name,
      pokemon: pokemon.data
    },
  };
};

export const getStaticPaths = async () => {
  const pokemons = await axios.get("https://pokeapi.co/api/v2/pokemon");
  const { data } = pokemons;
  const paths = data.results.map((pokemon) => ({
    params: { name: pokemon.name },
  }));

  return { paths, fallback: false };
};

export default PokemonById;
