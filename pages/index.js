import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import axios from "axios";
import Link from "next/link"

export default function Home({ data }) {
  return (
    <div className={styles.container}>
      <h1>Daftar Pokemon Zaky</h1>
      {data.map((item, index) => {
        return (
          <Link href={`${item.name}`} key={index} passHref>
          <div className={styles.card} key={index}>
              <h2>{item.name}</h2>
          </div>
           </Link>
        );
      })}
    </div>
  );
}

export const getStaticProps = async () => {
  const pokemons = await axios.get("https://pokeapi.co/api/v2/pokemon");
  const { data } = pokemons;
  return {
    props: {
      data: data.results,
    },
  };
};
